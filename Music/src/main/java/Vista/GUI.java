/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Controller;
import Modelo.Clases.Cancion;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class GUI extends javax.swing.JFrame {

    /**
     * Creates new form GUI
     */
    public GUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton1 = new javax.swing.JRadioButton();
        CONSULTAR = new javax.swing.JTabbedPane();
        PanelConsultar = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TConsultar = new javax.swing.JTable();
        LabelNombre = new javax.swing.JLabel();
        TFNombreConsultar = new javax.swing.JTextField();
        LabelArtista = new javax.swing.JLabel();
        TFArtistaConsultar = new javax.swing.JTextField();
        LabelGenero = new javax.swing.JLabel();
        CBGeneroConsultar = new javax.swing.JComboBox<>();
        btnConsultaradmin = new javax.swing.JButton();
        btnRegistraradmin = new javax.swing.JButton();
        btnActualizaradmin = new javax.swing.JButton();
        LabelId = new javax.swing.JLabel();
        TFIDConsultar = new javax.swing.JTextField();
        LabelRespuestaConsultar = new javax.swing.JLabel();
        btnEliminaradmin = new javax.swing.JButton();
        PanelRegistrar = new javax.swing.JPanel();
        TFNombreRegistrar = new javax.swing.JTextField();
        TFArtistaRegistrar = new javax.swing.JTextField();
        CBGeneroRegistrar = new javax.swing.JComboBox<>();
        LabelTituloRegistrar = new javax.swing.JLabel();
        LabelNombreRegistrar = new javax.swing.JLabel();
        LabelArtistaRegistrar = new javax.swing.JLabel();
        LabelGeneroRegistrar = new javax.swing.JLabel();
        btnRegistrar = new javax.swing.JButton();
        LabelRespuestaRegistrar = new javax.swing.JLabel();
        btnEscucharUsuario = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CONSULTAR.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        PanelConsultar.setBackground(new java.awt.Color(51, 102, 255));

        TConsultar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Artista", "Genero"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TConsultarMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TConsultar);

        LabelNombre.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelNombre.setText("Nombre:");

        TFNombreConsultar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        LabelArtista.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelArtista.setText("Artista:");

        TFArtistaConsultar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        TFArtistaConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFArtistaConsultarActionPerformed(evt);
            }
        });

        LabelGenero.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelGenero.setText("Genero:");

        CBGeneroConsultar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        CBGeneroConsultar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "BaladaPop", "Rock", "Ronda", "Bachata", "Salsa", "Merengue", "Reggaeton", " " }));
        CBGeneroConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CBGeneroConsultarActionPerformed(evt);
            }
        });

        btnConsultaradmin.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnConsultaradmin.setText("Consultar");
        btnConsultaradmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaradminActionPerformed(evt);
            }
        });

        btnRegistraradmin.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnRegistraradmin.setText("Registrar");
        btnRegistraradmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistraradminActionPerformed(evt);
            }
        });

        btnActualizaradmin.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnActualizaradmin.setText("Actualizar");
        btnActualizaradmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizaradminActionPerformed(evt);
            }
        });

        LabelId.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelId.setText("Id:");

        TFIDConsultar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        LabelRespuestaConsultar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        btnEliminaradmin.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnEliminaradmin.setText("Eliminar");

        javax.swing.GroupLayout PanelConsultarLayout = new javax.swing.GroupLayout(PanelConsultar);
        PanelConsultar.setLayout(PanelConsultarLayout);
        PanelConsultarLayout.setHorizontalGroup(
            PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConsultarLayout.createSequentialGroup()
                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelConsultarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(PanelConsultarLayout.createSequentialGroup()
                        .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelConsultarLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LabelNombre, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelConsultarLayout.createSequentialGroup()
                                        .addComponent(LabelId)
                                        .addGap(9, 9, 9))
                                    .addComponent(LabelArtista)
                                    .addComponent(LabelGenero)))
                            .addGroup(PanelConsultarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnConsultaradmin)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnActualizaradmin)
                                .addGap(18, 18, 18)
                                .addComponent(btnRegistraradmin)
                                .addGap(18, 18, 18)
                                .addComponent(btnEliminaradmin)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(PanelConsultarLayout.createSequentialGroup()
                .addGap(219, 219, 219)
                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TFIDConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelRespuestaConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TFNombreConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TFArtistaConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CBGeneroConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(155, Short.MAX_VALUE))
        );
        PanelConsultarLayout.setVerticalGroup(
            PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConsultarLayout.createSequentialGroup()
                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelConsultarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(70, 70, 70))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelConsultarLayout.createSequentialGroup()
                        .addGap(0, 123, Short.MAX_VALUE)
                        .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LabelId)
                            .addComponent(TFIDConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(LabelRespuestaConsultar)
                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelConsultarLayout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(CBGeneroConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelConsultarLayout.createSequentialGroup()
                        .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LabelNombre)
                            .addComponent(TFNombreConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LabelArtista)
                            .addComponent(TFArtistaConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(LabelGenero)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addGroup(PanelConsultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultaradmin)
                    .addComponent(btnActualizaradmin)
                    .addComponent(btnRegistraradmin)
                    .addComponent(btnEliminaradmin))
                .addGap(48, 48, 48))
        );

        CONSULTAR.addTab("Administrador", PanelConsultar);

        PanelRegistrar.setBackground(new java.awt.Color(51, 153, 255));

        TFNombreRegistrar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        TFArtistaRegistrar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        CBGeneroRegistrar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        CBGeneroRegistrar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pop", "Ranchera", "Ronda", "Bachata", "Salsa", "Merengue", "Reggaeton", "Popular", "Rock" }));
        CBGeneroRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CBGeneroRegistrarActionPerformed(evt);
            }
        });

        LabelTituloRegistrar.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LabelTituloRegistrar.setText("MUSIC STORE");

        LabelNombreRegistrar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelNombreRegistrar.setText("Nombre:");

        LabelArtistaRegistrar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelArtistaRegistrar.setText("Artista:");

        LabelGeneroRegistrar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        LabelGeneroRegistrar.setText("Genero:");

        btnRegistrar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        LabelRespuestaRegistrar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        btnEscucharUsuario.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnEscucharUsuario.setText("Escuchar");

        jButton1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jButton1.setText("Cambiar");

        javax.swing.GroupLayout PanelRegistrarLayout = new javax.swing.GroupLayout(PanelRegistrar);
        PanelRegistrar.setLayout(PanelRegistrarLayout);
        PanelRegistrarLayout.setHorizontalGroup(
            PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelRegistrarLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(LabelRespuestaRegistrar)
                .addGap(266, 266, 266))
            .addGroup(PanelRegistrarLayout.createSequentialGroup()
                .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PanelRegistrarLayout.createSequentialGroup()
                            .addGap(69, 69, 69)
                            .addComponent(btnEscucharUsuario)
                            .addGap(40, 40, 40)
                            .addComponent(btnRegistrar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                            .addComponent(jButton1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PanelRegistrarLayout.createSequentialGroup()
                            .addGap(129, 129, 129)
                            .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(LabelArtistaRegistrar)
                                .addComponent(LabelNombreRegistrar)
                                .addComponent(LabelGeneroRegistrar))
                            .addGap(62, 62, 62)
                            .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(TFNombreRegistrar)
                                .addComponent(TFArtistaRegistrar)
                                .addComponent(CBGeneroRegistrar, 0, 238, Short.MAX_VALUE))))
                    .addGroup(PanelRegistrarLayout.createSequentialGroup()
                        .addGap(215, 215, 215)
                        .addComponent(LabelTituloRegistrar)))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        PanelRegistrarLayout.setVerticalGroup(
            PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelRegistrarLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(LabelTituloRegistrar)
                .addGap(37, 37, 37)
                .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelRegistrarLayout.createSequentialGroup()
                        .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LabelNombreRegistrar)
                            .addComponent(TFNombreRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addComponent(LabelArtistaRegistrar))
                    .addComponent(TFArtistaRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelGeneroRegistrar)
                    .addComponent(CBGeneroRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addGroup(PanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEscucharUsuario)
                    .addComponent(btnRegistrar)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addComponent(LabelRespuestaRegistrar)
                .addGap(26, 26, 26))
        );

        CONSULTAR.addTab("Usuario", PanelRegistrar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CONSULTAR, javax.swing.GroupLayout.PREFERRED_SIZE, 639, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(CONSULTAR, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        this.LabelRespuestaRegistrar.setText("");
        String nombre = this.TFNombreRegistrar.getText();
        String artista = this.TFArtistaRegistrar.getText();
        String genero = this.CBGeneroRegistrar.getSelectedItem().toString();

        if (nombre.length() == 0 || artista.length() == 0) {
            this.LabelRespuestaRegistrar.setText("Ingrese valores válidos");
        } else {
            try {
                Controller.registrarCancion(nombre, artista, genero);
                this.LabelRespuestaRegistrar.setText("Valor registrado con éxito");
                this.TFNombreRegistrar.setText("");
                this.TFArtistaRegistrar.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                this.LabelRespuestaRegistrar.setText("Error");
            }
        }
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnConsultaradminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaradminActionPerformed
        String idString = this.TFIDConsultar.getText();
        DefaultTableModel modelo = (DefaultTableModel) this.TConsultar.getModel();
        modelo.setRowCount(0);
        if (idString.length() == 0) {
            List<Cancion> listaCanciones = Controller.listarCanciones();
            for (int i = 0; i < listaCanciones.size(); i++) {
                Cancion c1 = listaCanciones.get(i);
                modelo.insertRow(i, new Object[]{c1.getID(), c1.getNombre(), c1.getArtista(), c1.getGenero()});
            }
        } else {
            int id = Integer.valueOf(idString);
            Cancion c2 = Controller.obtenerCancion(id);
            modelo.insertRow(0, new Object[]{c2.getID(), c2.getNombre(), c2.getArtista(), c2.getGenero()});
        }
    }//GEN-LAST:event_btnConsultaradminActionPerformed

    private void TFArtistaConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFArtistaConsultarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFArtistaConsultarActionPerformed

    private void btnRegistraradminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistraradminActionPerformed
        String ID = this.TFIDConsultar.getText();
//        String nombre = this.TFNombreConsultar.getText();
//        String artista = this.TFArtistaConsultar.getText();
//        String genero = this.CBGeneroConsultar.getSelectedItem().toString();

        if (ID.length() == 0) {
            this.LabelRespuestaConsultar.setText("Ingrese valores válidos");
        } else {
            Controller.borrarCancion(Integer.valueOf(ID));
            this.LabelRespuestaConsultar.setText("Borrado exitoso");
            this.TFIDConsultar.setText("");
            this.TFNombreConsultar.setText("");
            this.TFArtistaConsultar.setText("");
//            this.CBGeneroConsultar.setSelectedItem("Pop");
        }
    }//GEN-LAST:event_btnRegistraradminActionPerformed

    private void btnActualizaradminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizaradminActionPerformed
        
        String ID = this.TFIDConsultar.getText();
        String nombre = this.TFNombreConsultar.getText();
        String artista = this.TFArtistaConsultar.getText();
        String genero = this.CBGeneroConsultar.getSelectedItem().toString();

        if (ID.length() == 0 || nombre.length() == 0 || artista.length() == 0) {
            this.LabelRespuestaConsultar.setText("Ingrese valores válidos");
        } else {
            Controller.actualizarCancion(Integer.valueOf(ID), nombre, artista, genero);
            this.LabelRespuestaConsultar.setText("Actualizacion exitosa");
            this.TFIDConsultar.setText("");
            this.TFNombreConsultar.setText("");
            this.TFArtistaConsultar.setText("");
        }
    }//GEN-LAST:event_btnActualizaradminActionPerformed

    private void TConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TConsultarMouseClicked
        JTable table = (JTable) evt.getSource();
        int row = table.rowAtPoint(evt.getPoint());
        DefaultTableModel modelo = (DefaultTableModel) this.TConsultar.getModel();

        String ID = String.valueOf(modelo.getValueAt(row, 0));
        String nombre = String.valueOf(modelo.getValueAt(row, 1));
        String artista = String.valueOf(modelo.getValueAt(row, 2));
        String genero = String.valueOf(modelo.getValueAt(row, 3));

        this.TFIDConsultar.setText(ID);
        this.TFNombreConsultar.setText(nombre);
        this.TFArtistaConsultar.setText(artista);
        this.CBGeneroConsultar.setSelectedItem(genero);
    }//GEN-LAST:event_TConsultarMouseClicked

    private void CBGeneroRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CBGeneroRegistrarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CBGeneroRegistrarActionPerformed

    private void CBGeneroConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CBGeneroConsultarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CBGeneroConsultarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CBGeneroConsultar;
    private javax.swing.JComboBox<String> CBGeneroRegistrar;
    private javax.swing.JTabbedPane CONSULTAR;
    private javax.swing.JLabel LabelArtista;
    private javax.swing.JLabel LabelArtistaRegistrar;
    private javax.swing.JLabel LabelGenero;
    private javax.swing.JLabel LabelGeneroRegistrar;
    private javax.swing.JLabel LabelId;
    private javax.swing.JLabel LabelNombre;
    private javax.swing.JLabel LabelNombreRegistrar;
    private javax.swing.JLabel LabelRespuestaConsultar;
    private javax.swing.JLabel LabelRespuestaRegistrar;
    private javax.swing.JLabel LabelTituloRegistrar;
    private javax.swing.JPanel PanelConsultar;
    private javax.swing.JPanel PanelRegistrar;
    private javax.swing.JTable TConsultar;
    private javax.swing.JTextField TFArtistaConsultar;
    private javax.swing.JTextField TFArtistaRegistrar;
    private javax.swing.JTextField TFIDConsultar;
    private javax.swing.JTextField TFNombreConsultar;
    private javax.swing.JTextField TFNombreRegistrar;
    private javax.swing.JButton btnActualizaradmin;
    private javax.swing.JButton btnConsultaradmin;
    private javax.swing.JButton btnEliminaradmin;
    private javax.swing.JButton btnEscucharUsuario;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnRegistraradmin;
    private javax.swing.JButton jButton1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
